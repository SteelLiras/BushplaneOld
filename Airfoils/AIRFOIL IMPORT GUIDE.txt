AIRFOIL IMPORT GUIDE
================================================

1. Download the XFoil compatible coordinates file
2. Load the airfoil into XFoil (LOAD filename)
3. Run GDES, CADD, \n\n\n\n
4. Run PANE
5. Save the smoothed airfoil (SAVE filename)
6. Open in Excel
7. Text to Columns
8. Add third (Z) column with a value of 0
9. Check the first and the last point and fix if they aren't coincident
10. Save as tab-delimited TXT